$(document).ready(function(){
	initLastLine();
});

function initLastLine(){
	$('#console_self').find('input').prop('disabled',true);
	$('#console_self').find('input:last').val("");	
	$('#console_self').find('input:last').prop('disabled',false);	
	$('#console_self').find('input:last').focus();	
	$('#console_self').find('input:last').keydown(function(event){
		if (event.which == 13){
			processInput($(this).val());
		}
		else if (event.which == 38){
			previousCommand();
		}
		else if (event.which == 40){
			nextCommand();
		}
	});
}

function processInput($input){
	if ( $input.trim() != "" ){
		$('.history').find('input.last').removeClass('last');
		$('.history').find('input:last').clone().insertBefore( $('.history').find('input:last') );
		$('.history').find('input:last').prev().val($input);
		$('.history').find('input:last').addClass('last');
	}
	$input = $input.trim();
	switch($input){
		case "" : newLine(); break;
		case "clear" : clear(); break;
		case "exit" : window.location.href = "/"; break;
		default : sendInput($input);
	}
}

function sendInput($input){
	$.ajax({
		url: "/console/command",
		data: {command:$input},
		method: "POST",
		dataType: "json",
		success: function($response){
			if ($response.status == 0 || $response.status == 1){
				$output_text="<label>";
				for ($i = 0; $i < $response.output.length; $i++){
					$output_text += $response.output[$i] + "<br>";	
				}
				$output_text+="</label>";
				$output = $('.output:last').clone();
				$('#console_self').append($output);
				$('#console_self').find('.output:last').show();
				$('#console_self').find('.output:last').html($output_text);
				newLine();
			}	
			else if ($response.status == 2){
				$('#console_self').append("redirecting...");
				window.location.href = $response.output;
			}
		} 		
	});	
}

function previousCommand(){
	if ( $('.history').find('input.last').prev().length > 0 ){
		$('#console_self').find('input:last').val( $('.history').find('input.last').prev().val() ); 
		$('.history').find('input.last').removeClass('last').prev().addClass('last');
	}
}

function nextCommand(){
	if ( $('.history').find('input.last').next().length > 0 ){
		$('#console_self').find('input:last').val( $('.history').find('input.last').next().val() );
		$('.history').find('input.last').removeClass('last').next().addClass('last');
	}
}

function newLine(){
	$line = $('.line:last').clone();
	$('#console_self').append($line);
	initLastLine();
}

function clear(){
	$('#console_self').find('.line:first').val("");
	$line = $('#console_self').find('.line:first');
	$('#console_self').find('.line').remove();
	$('#console_self').find('.output').remove();
	$('#console_self').append($line);
	initLastLine();
}
