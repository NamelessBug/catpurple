<?php 
class Entry_model extends CI_Model {
	
	public function is_editable($cookie_hash){

		$query = 'SELECT id	FROM entries e 
					JOIN quick_edit qe ON e.id = qe.entry_id
					WHERE cookie_hash = "'.$cookie_hash.'"
					AND (TIME_TO_SEC(CURRENT_TIMESTAMP) - TIME_TO_SEC(insert_time)) / 60 < '.QUICK_EDIT_TIME.'
					ORDER BY e.id  DESC';
		return (bool) $this->db->query($query)->conn_id->affected_rows;

	}

	
	public function insertQuickEditLine($entry_id){

		do{
			$cookie_hash = generate_hash(32);
			$query = 'SELECT * FROM quick_edit WHERE BINARY cookie_hash = "' . $cookie_hash . '"'; # BINARY makes the comparison case sensitive!
			$hash_existant = $this->db->query($query)->conn_id->affected_rows;
		}while ($hash_existant);

		$query = 'INSERT INTO quick_edit (entry_id,cookie_hash) VALUES ('.$entry_id.',"'.$cookie_hash.'")';
		if (!$this->db->query($query)){
			return array("success"=>false,"message"=>$this->db->conn_id->error,"exception_code"=>$this->db->conn_id->errno,
			"query"=>$query);
		}
		return array("success"=>true,
					"cookie_hash"=>$cookie_hash);

	}
	
}
