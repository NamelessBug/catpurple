<?php 
class Page_model extends CI_Model {
	
	
	public function save_entry($data)
	{
		if (strlen($data['content']) > MAX_ENTRY_LENGTH){
			redirect('https://en.wikipedia.org/wiki/Mentirosa');
		}
		
		$data['content'] = trim($data['content']," \t\n\r\0\x0B");
		$data['content'] = strip_tags($data['content'], '<br><b><i><h1><h2><h3><h4><h5><h6><hr><a><p>');
		$data['content'] = $this->remove_opening_tags($data['content']);
		$data['content'] = nl2br($data['content']);
		$animal_id 	 = mysqli_real_escape_string($this->db->conn_id,$data['animal_id']);
		$color_id 	 = mysqli_real_escape_string($this->db->conn_id,$data['color_id']);
		$position 	 = mysqli_real_escape_string($this->db->conn_id,$data['position']);
		$remote_ip  	 = $_SERVER['REMOTE_ADDR'];
		$remote_port  	 = $_SERVER['REMOTE_PORT'];
		$user_agent  	 = $_SERVER['HTTP_USER_AGENT'];
		
		if ($data['content'] != ""){
			$query = "INSERT INTO entries (animal_id, color_id, position, content, remote_ip, remote_port, user_agent) VALUES ('".$animal_id."','".$color_id."','".$position."',
			'".mysqli_real_escape_string($this->db->conn_id,$data['content'])."','".$remote_ip."','".$remote_port."','".$user_agent."')";
			if($this->db->conn_id->query($query)){
				
				return array("success"=>true,"content"=>$data['content'],"insert_id"=>$this->db->insert_id());
				
			}else{
				
				if ($this->db->conn_id->errno == DUPLICATE_ENTRY_CODE){
					
					$this->insertSomewhereElse($animal_id,$color_id,$position,$data);						
					
				}
				else{
					return array("success"=>false,"message"=>$this->db->conn_id->error,"exception_code"=>$this->db->conn_id->errno,
					"query"=>$query);
				}

			}
		}
		else{
			return array("success"=>false,"message"=>"El mensaje estaba vacío","exception_code"=>EMPTY_MESSAGE_ERROR);
		}
	}
	
	public function get_animal_id($animal_name){
		
		$query_string = "SELECT id FROM animals WHERE name = '".mysqli_real_escape_string($this->db->conn_id,$animal_name)."'";
		
		$query = $this->db->query($query_string);
		$row = $query->row();
		
		if (isset($row)){
			return $row->id;
		}
		else{
			return NULL_VALUE;
		}
		
	}
	
	public function get_color_id($color_name){
		
		$query_string = "SELECT id FROM colors WHERE name = '".mysqli_real_escape_string($this->db->conn_id,$color_name)."'";
		
		$query = $this->db->query($query_string);
		$row = $query->row();
		
		if (isset($row)){
			return $row->id;
		}
		else{
			return NULL_VALUE;
		}
		
	}
	
	public function get_animal_name($animal_id){
		
		$query_string = "SELECT name FROM animals WHERE id = '".mysqli_real_escape_string($this->db->conn_id,$animal_id)."'";
		
		$query = $this->db->query($query_string);
		$row = $query->row();
		
		if (isset($row)){
			return $row->name;
		}
		else{
			return NULL_VALUE;
		}
		
	}
	
	public function get_color_name($color_id){
		
		$query_string = "SELECT name FROM colors WHERE id = '".mysqli_real_escape_string($this->db->conn_id,$color_id)."'";
		
		$query = $this->db->query($query_string);
		$row = $query->row();
		
		if (isset($row)){
			return $row->name;
		}
		else{
			return NULL_VALUE;
		}
		
	}
	
	public function get_entries($data){
		
		$query_string = "SELECT content,position FROM entries WHERE animal_id = ".mysqli_real_escape_string($this->db->conn_id,$data['animal_id']).
		" AND color_id = " . mysqli_real_escape_string($this->db->conn_id,$data['color_id']) . 
		" AND position >= ".$data['from']." AND position <= " . ($data['to']) . ";";
		
		$query = $this->db->query($query_string);
		
		$result = $query->result_array();
		
		$entries = array();
		foreach ($result as $entry){
			$entries[$entry['position']] = $entry['content'];
		}
		return $entries;
	}
		
	public function get_entry($animal, $color, $position){
			
		if (!is_numeric($animal)){
			$animal = $this->get_animal_id($animal);
		}
		if (!is_numeric($color)){
			$color = $this->get_color_id($color);
		}

		if ($animal == NULL_VALUE){
			return array(
				"status" => 1,
				"error_type" => NON_EXISTANT_ANIMAL, 
				"message" => "the animal doesn't exist"
			);		
		}
		if ($color == NULL_VALUE){
			return array(
				"status" => 1,
				"error_type" => NON_EXISTANT_COLOR, 
				"message" => "the color doesn't exist"
			);		
		}	

		$query_string = "SELECT content, insert_time FROM entries WHERE animal_id = ".mysqli_real_escape_string($this->db->conn_id,$animal).
		" AND color_id = " . mysqli_real_escape_string($this->db->conn_id,$color) . 
		" AND position = " . mysqli_real_escape_string($this->db->conn_id,$position);
		
		$query = $this->db->query($query_string);
		$row = $query->row_array();
		
		if (isset($row)){
			return array(
				"status" => 0,
				"entry" => $row
			);		
		}
		else{
			return array(
				"status" => 1,
				"error_type" => ENTRY_NOT_FOUND, 
				"message" => "the entry was not found"
			);		
		}
		
	}	

	private function insertSomewhereElse($animal_id,$color_id,$position,$data){
		
		$entries_number = $this->db->query("SELECT count(content) as entries_number FROM entries")->row_array()['entries_number'];
		
		if ($entries_number < TOTAL_POSSIBLE_ENTRIES ){
			
			
			$counter = 0;
			do {
			
				$position++;
				if ($position > ENTRIES_PER_PAGE ){
					$position = 1;
					$color_id++;
				}
				if ($color_id > COLORS_NUMBER ){
					$color_id = 1;
					$animal_id++;
				}
				if ($animal_id > ANIMALS_NUMBER ){
					$animal_id = 1;								
				}
				
				$query = "SELECT content FROM entries WHERE animal_id=".$animal_id." AND color_id=".$color_id." AND position=".$position;
				
			}while (!empty($this->db->query($query)->row() || $counter == TOTAL_POSSIBLE_ENTRIES));
			
			if ($counter < TOTAL_POSSIBLE_ENTRIES){
				
				$query = "INSERT INTO entries (animal_id, color_id, position, content) VALUES ('".$animal_id."','".$color_id."','".$position."',
				'".mysqli_real_escape_string($this->db->conn_id,$data['content'])."')";
				
				if($this->db->conn_id->query($query)){
					
					$animal_name = $this->get_animal_name($animal_id);
					$color_name = $this->get_color_name($color_id);
													
					return array("success"=>true,"content"=>$data['content'],"exception_code"=>POSITION_TAKEN_WARNING,
					"new_entry_location"=>array("animal_name"=>$animal_name,"color_name"=>$color_name,"position"=>$position),"insert_id"=>$this->db->insert_id());
		
					
				}else{
					
					return array("success"=>false,"message"=>$this->db->conn_id->error,"exception_code"=>UNKNOWN_ERROR);
					
				}
				
				
			}
			
		}

	}

	private function remove_opening_tags($text){
		$tags = array("h1","h2","h3","h4","h5","h6","i","b","a","p");
		$checking_array = array();
		foreach ($tags as $tag){
			$checking_array[$tag] = 0;
		}
		$index = strlen($text) - 3;
		$last_closing_tag_char_index = -1;
		while($index >= 0){
			if ($text[$index] == '>'){
				$last_closing_tag_char_index = $index;
			}
			else if ($text[$index] == '<'){
				//primero chequelo la etiqueta de cierre (</h1>)
				$tag_found = false;
				foreach ($tags as $tag){
					if ( strlen($text) - $index >= strlen($tag) + 3){
						if ( substr($text,$index,strlen($tag)+3) == "</".$tag.">" ){
							$checking_array[$tag]++;
							$tag_found = true;
						}
					}
				}
				if (!$tag_found){
					//si no encontró de cierre, chequeo si hay de apertura
					foreach ($tags as $tag){
						if ( strlen($text) - $index >= strlen($tag) + 2){
							if ( substr($text,$index,strlen($tag) + 1) == "<".$tag && $last_closing_tag_char_index < strlen($text) ){
								if ($checking_array[$tag] > 0){
									$checking_array[$tag]--;
								}
								else{
									//remuevo la etiqueta de apertura
									$tag_length = $last_closing_tag_char_index - $index + 1;
									$text 	= substr($text,0,$index) . substr($text,$index+$tag_length,strlen($text)-$index-$tag_length);
								}
							}
							else{
							}
						}
					}
				}
			}
			$index--;
		}
		return $text;
	}

	public function insertQuickEditLine($entry_id){

		do{
			$cookie_hash = generate_hash(32);
			$query = 'SELECT * FROM quick_edit WHERE BINARY cookie_hash = "' . $cookie_hash . '"'; # BINARY makes the comparison case sensitive!
			$hash_existant = $this->db->query($query)->conn_id->affected_rows;
		}while ($hash_existant);

		$query = 'INSERT INTO quick_edit (entry_id,cookie_hash) VALUES ('.$entry_id.',"'.$cookie_hash.'")';
		if (!$this->db->query($query)){
			return array("success"=>false,"message"=>$this->db->conn_id->error,"exception_code"=>$this->db->conn_id->errno,
			"query"=>$query);
		}
		return array("success"=>true,
					"cookie_hash"=>$cookie_hash);

	}
	
	public function update_entry($data){
		
		$data['content'] = trim($data['content']," \t\n\r\0\x0B");
		$data['content'] = strip_tags($data['content'], '<br><b><i><h1><h2><h3><h4><h5><h6><hr><a><p>');
		$data['content'] = $this->remove_opening_tags($data['content']);
		$data['content'] = nl2br($data['content']);
		$query = 'UPDATE entries 
					SET content = "'.mysqli_real_escape_string($this->db->conn_id,$data['content']).'"
					WHERE animal_id = '.$data['animal_id'].' AND color_id = '.$data['color_id'].' AND position = '.$data['position'].'
					AND (TIME_TO_SEC(CURRENT_TIMESTAMP) - TIME_TO_SEC(insert_time)) / 60 < '.QUICK_EDIT_TIME*2;

		if ($this->db->query($query)){
			return true;
		}
		else{
			# logueo el query error
			return false; 
		}

	}
}
