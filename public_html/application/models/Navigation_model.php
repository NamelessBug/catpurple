<?php 
class Navigation_model extends CI_Model {

	public function get_previous_animal($animal_id)
	{
		$animal_query = "SELECT id,name FROM animals WHERE id < " . $animal_id . " AND enabled=1 ORDER BY id DESC LIMIT 1";
		$query_result = $this->db->query($animal_query)->row_array();
		if (empty($query_result)){
			$animal_query = "SELECT id,name FROM animals WHERE enabled=1 ORDER BY id DESC LIMIT 1";
			$query_result = $this->db->query($animal_query)->row_array();
		}
		if (!empty($query_result)){
			return $query_result;
		}
		else{
			// call a log function which should be available for all models. 
		}
	}
	public function get_next_animal($animal_id)
	{
		$animal_query = "SELECT id,name FROM animals WHERE id > " . $animal_id . " AND enabled=1 ORDER BY id LIMIT 1";
		$query_result = $this->db->query($animal_query)->row_array();
		if (empty($query_result)){
			$animal_query = "SELECT id,name FROM animals WHERE enabled=1 ORDER BY id LIMIT 1";
			$query_result = $this->db->query($animal_query)->row_array();
		}
		if (!empty($query_result)){
			return $query_result;
		}
		else{
			// call a log function which should be available for all models. 
		}
	}
			
	public function get_previous_color($color_id)
	{
		$color_query = "SELECT id,name FROM colors WHERE id < " . $color_id . " AND enabled=1 ORDER BY id DESC LIMIT 1";
		$query_result = $this->db->query($color_query)->row_array();
		if (empty($query_result)){
			$color_query = "SELECT id,name FROM colors WHERE enabled=1 ORDER BY id DESC LIMIT 1";
			$query_result = $this->db->query($color_query)->row_array();
		}
		if (!empty($query_result)){
			return $query_result;
		}
		else{
			// call a log function which should be available for all models. 
		}
	}

	public function get_next_color($color_id)
	{
		$color_query = "SELECT id,name FROM colors WHERE id > " . $color_id . " AND enabled=1 ORDER BY id LIMIT 1";
		$query_result = $this->db->query($color_query)->row_array();
		if (empty($query_result)){
			$color_query = "SELECT id,name FROM colors WHERE enabled=1 ORDER BY id LIMIT 1";
			$query_result = $this->db->query($color_query)->row_array();
		}
		if (!empty($query_result)){
			return $query_result;
		}
		else{
			// call a log function which should be available for all models. 
		}
	}

	public function get_previous_entry($position,$color_id,$animal_id)
	{
		$entry_query = "SELECT e.position as position,a.name as animal_name,c.name as color_name FROM entries e
							LEFT JOIN animals a ON e.animal_id = a.id 
							LEFT JOIN colors c ON e.color_id = c.id
							WHERE ((a.id = ".$animal_id." AND c.id = ".$color_id." AND position < ".$position.") OR
								(a.id = ".$animal_id." AND c.id < ".$color_id." ) OR
								(a.id < ".$animal_id." ))
							AND position < 1001
							ORDER BY a.id DESC, c.id DESC, e.position DESC LIMIT 1";
		$query_result = $this->db->query($entry_query)->row_array();
		if (empty($query_result)){
			$entry_query = "SELECT e.position as position,a.name as animal_name,c.name as color_name FROM entries e
								LEFT JOIN animals a ON e.animal_id = a.id 
								LEFT JOIN colors c ON e.color_id = c.id
								WHERE position < 1001
								ORDER BY a.id DESC, c.id DESC, e.position DESC LIMIT 1";
			$query_result = $this->db->query($entry_query)->row_array();
		}
		if (!empty($query_result)){
			return $query_result;
		}
		else{
			// call a log function which should be available for all models. 
		}
	}

	public function get_next_entry($position,$color_id,$animal_id)
	{
		$entry_query = "SELECT e.position as position,a.name as animal_name,c.name as color_name FROM entries e
							LEFT JOIN animals a ON e.animal_id = a.id 
							LEFT JOIN colors c ON e.color_id = c.id
							WHERE ((a.id = ".$animal_id." AND c.id = ".$color_id." AND position > ".$position.") OR
								(a.id = ".$animal_id." AND c.id > ".$color_id." ) OR
								(a.id > ".$animal_id." ))
							AND position < 1001
							ORDER BY a.id, c.id, e.position LIMIT 1";
		$query_result = $this->db->query($entry_query)->row_array();
		if (empty($query_result)){
			$entry_query = "SELECT e.position as position,a.name as animal_name,c.name as color_name FROM entries e
								LEFT JOIN animals a ON e.animal_id = a.id 
								LEFT JOIN colors c ON e.color_id = c.id
								WHERE position < 1001
								ORDER BY a.id, c.id, e.position LIMIT 1";
			$query_result = $this->db->query($entry_query)->row_array();
		}
		if (!empty($query_result)){
			return $query_result;
		}
		else{
			// call a log function which should be available for all models. 
		}
	}
}
