<?php 
class Script_model extends CI_Model {
	
	
	public function get_all_entries()
	{
		return $this->db->query("SELECT length(e.content), a.name as animal_name, c.name as color_name, e.animal_id, e.color_id, e.position, e.insert_time FROM entries e
			JOIN animals a ON a.id = e.animal_id
			JOIN colors c ON c.id = e.color_id
			ORDER BY insert_time DESC
		")->result_array();
	}
}
