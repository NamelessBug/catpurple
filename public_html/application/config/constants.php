<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('DOMAIN_ROOT')        		OR define('DOMAIN_ROOT', "http://catpurple.net/"); // no errors
defined('EXIT_SUCCESS')        		OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          		OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         		OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   		OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  		OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') 		OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     		OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       		OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      		OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      		OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*------DEFINED-BY-SANTIAGO-------*/
defined('ANIMALS_NUMBER')  			OR define('ANIMALS_NUMBER', 15); // 
defined('COLORS_NUMBER')  			OR define('COLORS_NUMBER', 15); // 
defined('MAX_ENTRIES_PER_LOAD')	 	OR define('MAX_ENTRIES_PER_LOAD', 100); // entries per page
defined('ENTRIES_PER_PAGE')  		OR define('ENTRIES_PER_PAGE', 1000); // max entries per load
defined('TOTAL_POSSIBLE_ENTRIES')  	OR define('TOTAL_POSSIBLE_ENTRIES', ANIMALS_NUMBER*COLORS_NUMBER*ENTRIES_PER_PAGE); // 
define('MAX_ENTRY_LENGTH', 34100); // largo máximo del string a insertar en la base de datos por cada entrada
define('EMPTY_MESSAGE_ERROR', 0); // código para cuando se intenta insertar un mensaje vacío
define('POSITION_TAKEN_WARNING', 1); // código para cuando la posición ya estaba ocupada
define('UNKNOWN_ERROR', 2); // código para cualquier error desconocido al insertar
define('DUPLICATE_ENTRY_CODE', 1062); // error al insertar por entrada duplicadad en mysql
define('PUBLIC_HIDDEN_ENTRIES', 341); // los 3 últimos dígitos para cualquier entrada oculta pública
define('PRIVATE_HIDDEN_ENTRIES', 347); // los 3 últimos dígitos para cualquier entrada oculta privada
define('NULL_VALUE', 'null_value'); // para cualquier valor nulo
define('QUICK_EDIT_TIME', 60); # en minutos

# codigos que puede devolver el método de un modelo en el campo error_type
define('NON_EXISTANT_ANIMAL','10');
define('NON_EXISTANT_COLOR','11');
define('ENTRY_NOT_FOUND','12');

# códigos que puede devolver la consola por ajax al javascript en el campo status
define('CONSOLE_NO_ERROR','0');
define('CONSOLE_ERROR','1');
define('CONSOLE_REDIRECT','2');

/*----LOG-TYPES----*/
define('LOG_GENERIC', '1');
define('LOG_BACKUP', '2');
define('LOG_TEST', '3');
define('LOG_MYSQL_ERROR', '4');
