<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="container" class="blue_bg">
	<div id="header-animals">
		<img src="/assets/img/headers.jpg" onload="show_container()">
	</div>
	<div id="content" class="w3-center">
		<div class="animals w3-center">
			<?php foreach ($animals as $animal){ ?>
				<a href="/<?php echo $animal['name']; ?>">
					<div class="animal w3-display-container <?php echo $animal['name']; ?>">
						<div class="name <?php echo $animal['name']; ?>"><span><?php echo strtoupper($animal['name']); ?></span></div>
						<img class="w3-display-middle" src="/assets/img/sprite-animals.png" onload="show_container()">
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</div>
<div id="loading" class="w3-display-container">
	<div class="w3-display-middle">
		<label>catpurple.net</label>
	</div>
</div>
<img class="image-loader" src="/assets/img/sprite-texts-bg.png">
<img class="image-loader" src="/assets/img/sprite-colors.png">
