<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="container" class="blue_bg">
	<div id="header-colors" class="w3-display-container">
		<div class="animal-img-container  w3-display-left"><img class="animal-img <?php echo $animal_name; ?>" src="/assets/img/sprite-animals.png"></div>
		<img class="title" src="/assets/img/headers.jpg" onload="show_container()">
	</div>
	<div id="content" class="w3-center">
		<div class="colors w3-center">
			<?php foreach ($colors as $color){ ?>
				<a href="<?php echo $animal_name . "/" . $color['name']; ?>">
					<div class="color w3-display-container <?php echo $color['name']; ?>">
						<img class="w3-display-middle" src="/assets/img/sprite-colors.png" onload="show_container()">
						<div class="name <?php echo $color['name']; ?>"><span><?php echo strtoupper($color['name']); ?></span></div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</div>
<div id="loading" class="w3-display-container">
	<div class="w3-display-middle">
		<label>catpurple.net</label>
	</div>
</div>
<img class="image-loader" src="/assets/img/sprite-texts-bg.png">

