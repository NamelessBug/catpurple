<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="container" class="console">
	<div id="console_self">
		<div class="line">
			<div class="prompt"><label>catpurple.net $</label></div>
			<div class="input_area"><input type="text" maxlength="80" value=""></div>
		</div>
	</div>
	<div class="output" style="display:none">
	</div>
	<div class="history">
		<input type="hidden" value="" class="last">
	</div>
</div>
