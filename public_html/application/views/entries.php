<div class="entries">
	<?php for ($i=$from; $i <= $from+MAX_ENTRIES_PER_LOAD-1; $i++){ ?>
		<?php if (isset($entries[$i])){ ?>
		<div class="entry <?php if (!isset($entries[$i])) echo "closed"; else echo "db"; echo " ".$color_name . " " . $i; ?>">
			<div class="number w3-display-container"><span class="w3-display-middle"><?php echo $i; ?></span></div>
			<div class="content">
				<div class="text w3-display-container">
					<label><?php if ($entries[$i] != "") echo $entries[$i]; else echo "&nbsp;" ?></label>
				</div>
			</div>
			<input type="hidden" class="position" value="<?php echo $i; ?>">
		</div>
		<?php } elseif (!$read_only) { ?>	
		<div class="entry <?php if (!isset($entries[$i])) echo "closed"; else echo "db"; echo " ".$color_name . " " . $i; ?>">
			<div class="number w3-display-container"><span class="w3-display-middle"><?php echo $i; ?></span></div>
			<div class="content">
				<div class="invitation"><span>Click to add some text</span></div>
				<div class="saving"><span>Saving...</span></div>
				<div class="inputs">
					<textarea spellcheck="false" maxlength="34100"></textarea>
					<input type="button" class="save" value="save">
					<input type="button" class="discard" value="discard">
				</div>			
			</div>
			<input type="hidden" class="position" value="<?php echo $i; ?>">
		</div>
		<?php } ?>
	<?php } ?>
</div>

