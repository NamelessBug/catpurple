<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Cat purple</title>
	<link rel=stylesheet type=text/css href="/assets/styles/w3.css">
	<link rel=stylesheet type=text/css href="/assets/styles/main.css">
	<?php if (isset($additional_css)) foreach ($additional_css as $path) { ?>
		<link rel=stylesheet type=text/css href="/assets/styles/<?php echo $path; ?>">
	<?php } ?>
	<script src="/assets/scripts/jquery-3.2.1.js" ></script>
	<?php if(isset($script_path)) { ?>
		<script src="/assets/scripts/<?php echo $script_path; ?>" ></script>	
	<?php } ?>
</head>
<body>
	<div id="wrapper">
		<div class="navigation-bar w3-display-container">
				<label class="left-arrow w3-display-left">
					<?php if (isset($previous_link)) { ?><a href="<?php echo $previous_link; ?>">&lt;</a><?php } ?>
				</label>
				<label class="w3-display-middle">
					<a href="/">catpurple.net</a>
					<?php if (isset($position)) {?>
						/ <a href="/<?php echo $animal_name; ?>"><?php echo $animal_name; ?></a>
						/ <a href="/<?php echo $animal_name."/".$color_name; ?>"><?php echo $color_name; ?></a>
						/ <?php echo $position; ?>
					<?php } elseif (isset($color_name)) {?>
						/ <a href="/<?php echo $animal_name; ?>"><?php echo $animal_name; ?></a>
						/ <?php echo $color_name; ?>
					<?php } elseif (isset($animal_name)) {?>
						/ <?php echo $animal_name; ?>
					<?php } ?>
					<?php if (isset($bc)) { echo $bc; } ?>
				</label>
				<label class="right-arrow w3-display-right">
					<?php if (isset($next_link)) { ?><a href="<?php echo $next_link; ?>">&gt;</a><?php } ?>
				</label>
		</div>
