<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Animals extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	
		$this->load->model('General_model','',TRUE);
		
		$data['script_path']    = "animals.js";
		
		$data['animals']        = $this->General_model->get_animals();
		$data['colors']         = $this->General_model->get_colors();
		
		$data['previous_link']  = "console";
		$data['next_link']      = "last100";
		
		$this->load->view('header',$data);
		$this->load->view('animals',$data);
		$this->load->view('footer',$data);
		
	}
}
