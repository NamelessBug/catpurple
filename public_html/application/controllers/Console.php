<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Console extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	var $current_command = "";

	public function index($all=false)
	{
		
		# $this->load->model('Console_model','',TRUE);
		$data = array();
	
		$data['next_link'] = "/";
		$data['script_path'] = "console.js";

		$this->load->view('header',$data);
		$this->load->view('console',$data);
		$this->load->view('footer',$data);
		
	}
	public function command(){
		
		$input 	= trim(explode(" ",$_POST['command'])[0]);
		$tokens = explode(" ",$_POST['command']);
		array_splice($tokens,0,1);
		for ($i = 0; $i < count($tokens); $i++){
			$tokens[$i] = trim($tokens[$i]);
		}

		$response = array(
						"status" => 1,	
						"output" => array("Command '" . $input . "' not found. Type 'help' to get some help."),
					);
		switch ($input) {
			case "help" : $response = $this->help(); break;
			case "ip" : $response = $this->ip(); break;
			case "show" : $response = $this->show($tokens); break;
			case "go" : $response = $this->go($tokens); break;
		}
		
		echo json_encode($response);
	
	}		
	public function help(){

		$this->current_command = "help";
		$output = array(
			"",
			"  -- CATPURPLE.NET -- ",
			"",
			"clear : clears the console",
			"ip : shows the public ip from which you are connecting",
			"help : shows this help",
			"go : displays a help message on the 'go' command",
			"show : displays any specified entry",
			""
		);
		return array(
			"status" => CONSOLE_NO_ERROR,
			"output" => $output
		);

	}	
	public function ip(){
		
		$this->current_command = "ip";
		$output = array(
			"your public ip is ".$_SERVER['REMOTE_ADDR']
		);
		return array(
			"status" => CONSOLE_NO_ERROR,
			"output" => $output
		);

	}
	public function show($tokens){
	
		$this->current_command = "show";

		$status = CONSOLE_NO_ERROR;
		if (isset($tokens[0])){
			$animal_name = $tokens[0];
			if (isset($tokens[1])){
				$color_name = $tokens[1];
				if (isset($tokens[2])){
					$position = $tokens[2];
				}
				else{
					return $this->error("position missing. must enter &lt;animal_name&gt; &lt;color_name&gt; &lt;position&gt;");
				}
			}
			else{
				return $this->error("color missing. must enter &lt;animal_name&gt; &lt;color_name&gt; &lt;position&gt;"); 
			}
		}	
		else{
			return $this->error("animal missing. must enter &lt;animal_name&gt; &lt;color_name&gt; &lt;position&gt;");
		}

		if (!is_numeric($position)){
			return $this->error("position must be a number. must enter &lt;animal_name&gt; &lt;color_name&gt; &lt;position&gt;");
		}

		$this->load->model('Page_model','',TRUE);
		$response = $this->Page_model->get_entry($animal_name,$color_name,$position);

		if ($response['status'] == 0){
			$output = array($response['entry']['content']);
		}	
		else{
			return $this->error($response['message']);
		}
	 
		return array(
			"status" => $status,
			"output" => $output
		);

	}

	public function go($tokens){
		
		$this->current_command = "go";
		$status = CONSOLE_REDIRECT;
		$this->load->model('Page_model','',TRUE);

		if (isset($tokens[0])){
			switch ($tokens[0]){
				case 'last100' : 
					$output = "/last100";
				break;
				default:

					$animal_name = $tokens[0];
					$animal_id = $this->Page_model->get_animal_id($animal_name);

					if ($animal_id != NULL_VALUE){

						if (isset($tokens[1])){

							$color_name = $tokens[1];
							$color_id = $this->Page_model->get_color_id($color_name);

							if ($color_id != NULL_VALUE){

								if (isset($tokens[2])){

									$position = $tokens[2];

									if (is_numeric($position)){

										if ($position > 0 && $position < 1001){
											$output = "/".$animal_name."/".$color_name."/".$position;
										}
										else{
											return $this->error("Position out of range");
										}

									}
									else{ # position is not numeric
										return $this->error("Third token must be a number");
									}
								}
								else{
									$output = "/".$animal_name."/".$color_name;			
								}
							}
							else{ # color_id == NULL
								return $this->error("Unknown token '".$color_name."'");
							}
						}
						else{ # ! isset tokens[1]
							$output = "/".$animal_name;
						}
					}
					else{ # animal_id == NULL
						return $this->error("Unknown token '".$animal_name."'");
					}
				break;	
			}
		}
		else{ # ! isset tokens[0]
			$status = CONSOLE_NO_ERROR;
			$output = array(
				"Go: Must specify where to go",
				"",
				"last100 : redirects to the last100 entries page",
				"&lt;animal&gt; : redirects to the specified animal page",
				"&lt;animal&gt; &lt;color&gt; : redirects to the specified animal/color page",
				"&lt;animal&gt; &lt;color&gt; &lt;position&gt; : redirects to the specified entry page"
			);
		}

		return array(
			"status" => $status,
			"output" => $output
		);

	}

	public function error($message){

		return array(
			"status" => CONSOLE_ERROR,
			"output" => array($this->current_command . ": " . $message)
		);

	}
}
