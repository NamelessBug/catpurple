<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($animal_name,$color_name,$read_only=false)
	{
		$this->load->model('Page_model','',TRUE);
		$this->load->model('Navigation_model','',TRUE);
		
		if (false){
			$animals = array(
				array(
					"name" => "rhinoceros",
					"xPoint" => 8876,
					"figure" => 10
				),
				array(
					"name" => "seal",
					"xPoint" => 9510,
					"figure" => 11
				),
				array(
					"name" => "scorpion",
					"xPoint" => 9714,
					"figure" => 12
				),
				array(
					"name" => "spider",
					"xPoint" => 10208,
					"figure" => 13
				),
				array(
					"name" => "swan",
					"xPoint" => 10545,
					"figure" => 14
				),
				array(
					"name" => "bear",
					"xPoint" => 10828,
					"figure" => 15
				),
				array(
					"name" => "butterfly",
					"xPoint" => 11074,
					"figure" => 16
				),
				array(
					"name" => "cat",
					"xPoint" => 11611,
					"figure" => 0
				),
				array(
					"name" => "deer",
					"xPoint" => 11788,
					"figure" => 1
				),
				array(
					"name" => "duck",
					"xPoint" => 12030,
					"figure" => 2
				),
				array(
					"name" => "elephant",
					"xPoint" => 12294,
					"figure" => 3
				),
				array(
					"name" => "flamingo",
					"xPoint" => 12790,
					"figure" => 4
				),
				array(
					"name" => "giraffe",
					"xPoint" => 13303,
					"figure" => 5
				),
				array(
					"name" => "gorilla",
					"xPoint" => 13697,
					"figure" => 6
				),
				array(
					"name" => "horse",
					"xPoint" => 14115,
					"figure" => 7
				),
				array(
					"name" => "lion",
					"xPoint" => 14433,
					"figure" => 8
				),
				array(
					"name" => "pig",
					"xPoint" => 14651,
					"figure" => 9
				),
				array(
					"name" => "void",
					"xPoint" => 14795,
					"figure" => 17
				)
			);
			
			
			for($i=0; $i < count($animals); $i++){
				
				
				if ($i < count($animals)-1 ){
					
					$startPoint = $animals[$i]["xPoint"];
					$endPoint 	= $animals[$i+1]["xPoint"]-1;
					
					echo '<b>'.$animals[$i]["name"].'</b><br>';
					
					$val1 = $startPoint/200;
					$val2 = $val1 * 10.5;
					
					$val3 = $endPoint - $startPoint;
					$val4 = $val3/200;
					$val5 = $val4 * 10.5;
					
					$val6 = $animals[$i]["figure"] * 234;
					$val7 = $val6/200;
					$val8 = $val7 * 10.5;
					
					echo $startPoint . " / 200 = " . $val1 . "; " . $val1 . " * 10.5 = " . $val2 . '<br>';
					echo $endPoint . " - " .$startPoint . " = " . $val3 . "; " . $val3 . " / 200 = " . $val4 . "; " . $val4 . " * 10.5 = " . $val5 . '<br>';
					echo $animals[$i]["figure"] . " * 234 = " . $val6 . "; " . $val6 . " / 200 = " . $val7 . "; " . $val7 . " * 10.5 = " . $val8 . '<br>';
					echo "figure: -" . $val8 . ";" . '<br>';
					echo "margin-left: -" . $val2 . ";" . '<br>';
					echo "width: " . $val5 . ";" . '<br>';
					echo '-------------------------------------------------------------------------------------<br>';
					
				}
				
			}
			
			die();
		}
		
		
		$data = array("animal_name"=>$animal_name,"color_name"=>$color_name);
		$data['animal_id'] 		= $this->Page_model->get_animal_id($animal_name);
		$data['color_id'] 		= $this->Page_model->get_color_id($color_name);
		$data['from'] 			= 1;
		$data['to'] 			= MAX_ENTRIES_PER_LOAD;
		$data['read_only']		= $read_only;
		
		$previous_color 		= $this->Navigation_model->get_previous_color($data['color_id']);
		if ($previous_color['id'] < $data['color_id']){
			$data['previous_link'] = "/".$animal_name."/".$previous_color['name'];
		}
		else{
			$previous_animal 	= $this->Navigation_model->get_previous_animal($data['animal_id']);
			$data['previous_link'] = "/".$previous_animal['name']."/".$previous_color['name'];
		}

		$next_color 		= $this->Navigation_model->get_next_color($data['color_id']);
		if ($next_color['id'] > $data['color_id']){
			$data['next_link'] = "/".$animal_name."/".$next_color['name'];
		}
		else{
			$next_animal 	= $this->Navigation_model->get_next_animal($data['animal_id']);
			$data['next_link'] = "/".$next_animal['name']."/".$next_color['name'];
		}

		if ($data['animal_id'] == NULL_VALUE || $data['color_id'] == NULL_VALUE ){
			redirect(DOMAIN_ROOT.'home');
		}
		
		$data['entries'] = $this->Page_model->get_entries($data);
		$data['script_path'] = "page.js";
		
		$this->load->view('header',$data);
		$this->load->view('page',$data);
		$this->load->view('footer',$data);
		
		
	}

	public function segment($animal_name, $color_name, $position_range, $read_only=false, $hidden=false){
			
		$this->load->model('Page_model','',TRUE);
		$this->load->model('Navigation_model','',TRUE);

		$pr = explode('-',$position_range);

		$data = array("animal_name"=>$animal_name,"color_name"=>$color_name);
		$data['animal_id'] 		= $this->Page_model->get_animal_id($animal_name);
		$data['color_id'] 		= $this->Page_model->get_color_id($color_name);
        $data['from'] 			= $pr[0];
        $data['to'] 			= $pr[1];
        $data['read_only']		= $read_only;

		$previous_color 		= $this->Navigation_model->get_previous_color($data['color_id']);
		if ($previous_color['id'] < $data['color_id']){
			$data['previous_link'] = "/".$animal_name."/".$previous_color['name'];
		}
		else{
			$previous_animal 	= $this->Navigation_model->get_previous_animal($data['animal_id']);
			$data['previous_link'] = "/".$previous_animal['name']."/".$previous_color['name'];
		}

		if ($data['animal_id'] == NULL_VALUE || $data['color_id'] == NULL_VALUE ){
			redirect(DOMAIN_ROOT.'home');
		}
		
        if ($hidden){
            $entries_big_index = $this->Page_model->get_entries($data);
            foreach ($entries_big_index as $key => $value){
                $remiainig = $key % 1000;
                $entries[ ($key - $remiainig) / 1000 ] = $value;
            }
            $data['from'] 			= 1;
            $data['to'] 			= 1000;
        }
        else{
            $entries = $this->Page_model->get_entries($data);
        }
		
		$data['entries'] = $entries;
		$data['script_path'] = "page.js";
		
		$this->load->view('header',$data);
		$this->load->view('segment',$data);
		$this->load->view('footer',$data);
	}
    
}

