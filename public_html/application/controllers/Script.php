<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$this->load->model('Script_model','',TRUE);
		
		$entries = $this->Script_model->get_all_entries();
		echo count($entries).'<hr>';
		foreach ($entries as $entry){
			echo '&lt;a target="blank_" href="/'.$entry['animal_name'].'/'.$entry['color_name'].'/'.$entry['position'].'"&gt;';
			echo $entry['animal_name'] . " > " . $entry['color_name'] . " > " . $entry['position'] . "&lt;/a&gt;<br>";
		}
	}
}
