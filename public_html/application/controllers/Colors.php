<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Colors extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($animal_name)
	{
		$this->load->model('General_model','',TRUE);
		$this->load->model('Page_model','',TRUE);
		$this->load->model('Navigation_model','',TRUE);
		
		$data['script_path']	= "colors.js";
		$animal_id 				= $this->Page_model->get_animal_id($animal_name);
		
		if ($animal_id == NULL_VALUE){
			redirect(DOMAIN_ROOT);
		}

		$data['animal_name']	= $animal_name;
		$data['colors'] 		= $this->General_model->get_colors();
	
		
		$previous_animal		= $this->Navigation_model->get_previous_animal($animal_id);
		$data['previous_link']	= "/".$previous_animal['name'];
		$next_animal			= $this->Navigation_model->get_next_animal($animal_id);
		$data['next_link']		= "/".$next_animal['name'];
		
		$this->load->view('header',$data);
		$this->load->view('colors',$data);
		$this->load->view('footer',$data);
		
	}
}
