#!/bin/bash

host=ftp.catpurple.net
user=dbrr56sa9@catpurple.net
echo "Enter password"
read -s password

today=$(date +"%Y_%m_%d")
cd /var/www/catpurple.dev/backups

if [ ! -d $today ]
then
	mkdir $today
fi

ftp -ivn <<2017_HD_EXIT_STRING
open $host
quote USER $user
quote PASS $password
passive

cd /cron/backups/
lcd /var/www/catpurple.dev/backups/$today

mget *
bye

2017_HD_EXIT_STRING

cd /var/www/catpurple.dev/backups/$today
echo $(ls -l | wc -l)" files downloaded"

for i in $(ls)
do
	cat $i >> all.bkp
	rm $i
done
